import express from 'express';
import movieRouter from './Routes/movieRouter.js';
import pokemonRouter from './Routes/pokemonRouter.js';

const port = process.env.PORT || 3000;
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api/movie', movieRouter);
app.use('/api/pokemon', pokemonRouter);

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server running on port ${port}`);
});
