import axios from 'axios';

import responseHelper from '../Helper/responseHelper.js';
import { getMyPokemon, setMyPokemon, generatePokemonId, renamePokemon } from '../Helper/pokemonHelper.js';

const pokemonController = {
  getPokemonList: async (req, res) => {
    try {
      const response = await axios.get('https://pokeapi.co/api/v2/pokemon?limit=100');
      const listPokemonName = response?.data?.results?.map((item) => item.name);
      responseHelper(res, 200, listPokemonName, 'Succesfully get pokemon list');
    } catch (error) {
      responseHelper(res, 500);
    }
  },
  getPokemonDetail: async (req, res) => {
    try {
      const response = await axios.get(`https://pokeapi.co/api/v2/pokemon/${req.params.name}`);
      responseHelper(res, 200, response.data, 'Succesfully get pokemon detail');
    } catch (error) {
      const errStatus = error?.response?.status;
      if (errStatus === 404) {
        responseHelper(res, 400, null, `Can't find pokemon with name: ${req.params.name}`);
      } else {
        responseHelper(res, 500);
      }
    }
  },
  getMyPokemon: (req, res) => {
    try {
      const myPokemon = getMyPokemon();
      responseHelper(res, 200, myPokemon, 'Succesfully get my pokemon');
    } catch (error) {
      responseHelper(res, 500);
    }
  },
  catchPokemon: (req, res) => {
    try {
      const myPokemon = getMyPokemon();
      const newPokemon = {
        id: generatePokemonId(),
        name: req.params.name,
        release: 0,
      };
      myPokemon.push(newPokemon);
      setMyPokemon(myPokemon);
      responseHelper(res, 200, { ...newPokemon, catchPokemon: true }, 'Succesfully catch pokemon');
    } catch (error) {
      responseHelper(res, 500);
    }
  },
  releasePokemon: (req, res) => {
    try {
      const { randomNumber } = res.locals;
      responseHelper(res, 200, { succesRelease: true }, `You got ${randomNumber}! Success to release pokemon`);
    } catch (error) {
      responseHelper(res, 500);
    }
  },

  renamePokemon: (req, res) => {
    try {
      const myPokemon = getMyPokemon();
      const selectedPokemon = myPokemon.find((item) => item.id === req.params.id);
      if (selectedPokemon) {
        const newPokemon = myPokemon.map((item) => {
          if (item.id === req.params.id) {
            item.name = renamePokemon(item.name, item.release);
            item.release += 1;
          }
          return item;
        });
        setMyPokemon(newPokemon);
        responseHelper(res, 200, selectedPokemon, `Success rename pokemon`);
      } else {
        responseHelper(res, 400, null, `cant find pokemon with id: ${req.params.id}`);
      }
    } catch (error) {
      responseHelper(res, 500);
    }
  },
};

export default pokemonController;
