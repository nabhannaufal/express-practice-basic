import { readFileSync, writeFileSync } from 'fs';
import { randomBytes } from 'crypto';
import responseHelper from '../../Helper/responseHelper';

import {
  getMyPokemon,
  setMyPokemon,
  generatePokemonId,
  gachaPokemon,
  checkRelease,
  renamePokemon,
} from '../../Helper/pokemonHelper';

import { isPrime, fibonacci } from '../../Helper/generalHelper';

jest.mock('fs');
jest.mock('crypto');
jest.mock('../../Helper/responseHelper');
jest.mock('../../Helper/generalHelper');

describe('pokemonUtils', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('getMyPokemon', () => {
    it('should read and parse data from the file', () => {
      const mockData = { myPokemon: [{ name: 'Pikachu' }] };
      readFileSync.mockReturnValue(JSON.stringify(mockData));

      const result = getMyPokemon();

      expect(result).toEqual(mockData.myPokemon);
      expect(readFileSync).toHaveBeenCalledWith(expect.anything());
    });
  });

  describe('setMyPokemon', () => {
    it('should write data to the file', () => {
      const mockData = [{ name: 'Pikachu' }];

      setMyPokemon(mockData);

      expect(writeFileSync).toHaveBeenCalledWith(expect.anything(), JSON.stringify({ myPokemon: mockData }));
    });
  });

  describe('generatePokemonId', () => {
    it('should generate a random hex string', () => {
      randomBytes.mockReturnValue({ toString: jest.fn(() => 'randomHexString') });

      const result = generatePokemonId();

      expect(result).toBe('randomHexString');
      expect(randomBytes).toHaveBeenCalledWith(16);
    });
  });

  describe('gachaPokemon', () => {
    it('should call next() if random number is 1', () => {
      const mockReq = {};
      const mockRes = {};
      const mockNext = jest.fn();

      // Mock Math.random() to always return 0.5 (for the sake of testing)
      global.Math.random = jest.fn(() => 0.5);

      gachaPokemon(mockReq, mockRes, mockNext);

      expect(mockNext).toHaveBeenCalled();
      expect(responseHelper).not.toHaveBeenCalled();
    });

    it('should call responseHelper with the correct arguments if random number is 0', () => {
      const mockReq = {};
      const mockRes = {};

      // Mock Math.random() to always return 0 (for the sake of testing)
      global.Math.random = jest.fn(() => 0);

      gachaPokemon(mockReq, mockRes);

      expect(responseHelper).toHaveBeenCalledWith(mockRes, 201, { catchPokemon: false }, 'Fail to catch pokemon');
    });
  });

  describe('checkRelease', () => {
    beforeEach(() => {
      jest.clearAllMocks(); // Clear mock function calls before each test
    });

    it('should call next() if the random number is prime', () => {
      const req = {};
      const res = {
        locals: {},
      };
      const next = jest.fn();

      isPrime.mockReturnValueOnce(true); // Mock isPrime to return true

      checkRelease(req, res, next);

      expect(isPrime).toHaveBeenCalledTimes(1);
      expect(res.locals.randomNumber).toBeDefined();
      expect(next).toHaveBeenCalledTimes(1);
    });

    it('should call responseHelper if the random number is not prime', () => {
      const req = {};
      const res = {};
      const next = jest.fn();

      isPrime.mockReturnValueOnce(false); // Mock isPrime to return false

      checkRelease(req, res, next);

      expect(isPrime).toHaveBeenCalledTimes(1);
      expect(responseHelper).toHaveBeenCalledWith(
        res,
        201,
        { succesRelease: false },
        expect.stringContaining('Failed to release pokemon')
      );
      expect(next).not.toHaveBeenCalled();
    });
  });

  describe('renamePokemon', () => {
    it('should return a correctly renamed Pokemon name', () => {
      const name = 'Pikachu';
      const release = 5;

      fibonacci.mockReturnValueOnce(5); // Mock fibonacci to return 5

      const newName = renamePokemon(name, release);

      expect(newName).toBe('Pikachu-5');
      expect(fibonacci).toHaveBeenCalledWith(release);
    });
  });
});
