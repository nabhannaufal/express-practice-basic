import movieController from '../../Controllers/movieController';
import responseHelper from '../../Helper/responseHelper';
import { getData, setData, generateMovieId } from '../../Helper/movieHelper';

jest.mock('../../Helper/responseHelper');
jest.mock('../../Helper/movieHelper');

describe('movieController', () => {
  let req;
  let res;

  beforeEach(() => {
    req = { params: {} };
    res = { status: jest.fn().mockReturnThis(), json: jest.fn() };
    jest.clearAllMocks();
  });

  describe('getMovie', () => {
    it('should get movies successfully', () => {
      const mockData = { movies: [{ id: 1, title: 'Movie 1' }] };
      getData.mockReturnValue(mockData);

      movieController.getMovie(req, res);

      expect(getData).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(res, 200, mockData);
    });

    it('should handle error and return 500', () => {
      getData.mockImplementation(() => {
        throw new Error('Some error');
      });

      movieController.getMovie(req, res);

      expect(getData).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(res, 500);
    });
  });

  describe('getMovieById', () => {
    it('should get a movie by id successfully', () => {
      const mockData = { movies: [{ id: 1, title: 'Movie 1' }] };
      getData.mockReturnValue(mockData);

      req.params.id = '1';
      movieController.getMovieById(req, res);

      expect(getData).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(res, 200, mockData.movies[0]);
    });

    it('should handle missing movie and return 400', () => {
      getData.mockReturnValue({ movies: [] });

      req.params.id = '1';
      movieController.getMovieById(req, res);

      expect(getData).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(res, 400, null, "Can't find movie with id: 1");
    });

    it('should handle error and return 500', () => {
      getData.mockImplementation(() => {
        throw new Error('Some error');
      });

      movieController.getMovieById(req, res);

      expect(getData).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(res, 500);
    });
  });

  describe('addMovie', () => {
    it('should add a new movie successfully', () => {
      const mockData = { movies: [] };
      getData.mockReturnValue(mockData);
      generateMovieId.mockReturnValue(1);

      req.body = { title: 'New Movie', year: 2023, genre: 'Action' };
      movieController.addMovie(req, res);

      expect(generateMovieId).toHaveBeenCalled();
      expect(getData).toHaveBeenCalled();
      expect(setData).toHaveBeenCalledWith({ movies: [{ id: 1, title: 'New Movie', year: 2023, genre: 'Action' }] });
      expect(responseHelper).toHaveBeenCalledWith(
        res,
        200,
        { id: 1, title: 'New Movie', year: 2023, genre: 'Action' },
        'Succesfully add new movie'
      );
    });

    it('should handle error and return 500', () => {
      getData.mockImplementation(() => {
        throw new Error('Some error');
      });

      movieController.addMovie(req, res);

      expect(getData).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(res, 500);
    });
  });

  describe('editMovie', () => {
    it('should edit a movie successfully', () => {
      const mockData = { movies: [{ id: 1, title: 'Old Movie', year: 2020, genre: 'Comedy' }] };
      getData.mockReturnValue(mockData);

      req.params.id = '1';
      req.body = { title: 'New Title', year: 2023, genre: 'Action' };
      movieController.editMovie(req, res);

      expect(getData).toHaveBeenCalled();
      expect(setData).toHaveBeenCalledWith({
        movies: [{ id: 1, title: 'New Title', year: 2023, genre: 'Action' }],
      });
      expect(responseHelper).toHaveBeenCalledWith(
        res,
        200,
        [{ id: 1, title: 'New Title', year: 2023, genre: 'Action' }],
        'Movie with id: 1 has been updated'
      );
    });

    it('should handle error and return 500', () => {
      getData.mockImplementation(() => {
        throw new Error('Some error');
      });

      movieController.editMovie(req, res);

      expect(getData).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(res, 500);
    });
  });

  describe('deleteMovie', () => {
    it('should delete a movie successfully', () => {
      const mockData = { movies: [{ id: 1, title: 'Movie 1' }] };
      getData.mockReturnValue(mockData);

      req.params.id = '1';
      movieController.deleteMovie(req, res);

      expect(getData).toHaveBeenCalled();
      expect(setData).toHaveBeenCalledWith({ movies: [] });
      expect(responseHelper).toHaveBeenCalledWith(res, 200, mockData.movies[0], 'Movie with id: 1 has been deleted');
    });

    it('should handle missing movie and return 400', () => {
      getData.mockReturnValue({ movies: [] });

      req.params.id = '1';
      movieController.deleteMovie(req, res);

      expect(getData).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(res, 400, null, 'Movie not found');
    });

    it('should handle error and return 500', () => {
      getData.mockImplementation(() => {
        throw new Error('Some error');
      });

      movieController.deleteMovie(req, res);

      expect(getData).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(res, 500);
    });
  });
});
