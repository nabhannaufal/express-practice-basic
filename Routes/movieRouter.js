import express from 'express';
import movieController from '../Controllers/movieController.js';

const movieRouter = express.Router();

movieRouter.get('/', movieController.getMovie);
movieRouter.get('/:id', movieController.getMovieById);
movieRouter.post('/', movieController.addMovie);
movieRouter.put('/:id', movieController.editMovie);
movieRouter.delete('/:id', movieController.deleteMovie);

export default movieRouter;
