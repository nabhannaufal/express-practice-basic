import responseHelper from '../Helper/responseHelper.js';
import { getData, setData, generateMovieId } from '../Helper/movieHelper.js';

const movieController = {
  getMovie: (req, res) => {
    try {
      const data = getData();
      responseHelper(res, 200, data);
    } catch (err) {
      responseHelper(res, 500);
    }
  },
  getMovieById: (req, res) => {
    try {
      const data = getData();
      const findMovie = data?.movies?.find((item) => item.id === Number(req.params.id));
      if (findMovie) {
        responseHelper(res, 200, findMovie);
      } else {
        responseHelper(res, 400, null, `Can't find movie with id: ${req.params.id}`);
      }
    } catch (err) {
      responseHelper(res, 500);
    }
  },
  addMovie: (req, res) => {
    try {
      const newMovie = {
        id: generateMovieId(),
        ...req.body,
      };
      const data = getData();
      setData({ movies: [...data.movies, newMovie] });
      responseHelper(res, 200, newMovie, 'Succesfully add new movie');
    } catch (err) {
      responseHelper(res, 500);
    }
  },
  editMovie: (req, res) => {
    try {
      const data = getData();
      const newMovie = data?.movies?.map((item) => {
        if (item.id === Number(req.params.id)) {
          item.title = req.body.title;
          item.year = req.body.year;
          item.genre = req.body.genre;
        }
        return item;
      });
      setData({ movies: newMovie });
      responseHelper(res, 200, newMovie, `Movie with id: ${req.params.id} has been updated`);
    } catch (err) {
      responseHelper(res, 500);
    }
  },
  deleteMovie: (req, res) => {
    try {
      const data = getData();
      const findMovie = data?.movies?.find((item) => item.id === Number(req.params.id));
      if (findMovie) {
        const newMovie = data.movies.filter((item) => item.id !== Number(req.params.id));
        setData({ movies: newMovie });
        responseHelper(res, 200, findMovie, `Movie with id: ${req.params.id} has been deleted`);
      } else {
        responseHelper(res, 400, null, 'Movie not found');
      }
    } catch (err) {
      responseHelper(res, 500);
    }
  },
};

export default movieController;
