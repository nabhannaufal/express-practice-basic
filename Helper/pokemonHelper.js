import { readFileSync, writeFileSync } from 'fs';
import { randomBytes } from 'crypto';
import responseHelper from './responseHelper.js';
import { isPrime, fibonacci } from './generalHelper.js';

const dbPath = new URL('../Database/myPokemon.json', import.meta.url);

export const getMyPokemon = () => JSON.parse(readFileSync(dbPath)).myPokemon;
export const setMyPokemon = (data) => writeFileSync(dbPath, JSON.stringify({ myPokemon: data }));
export const generatePokemonId = () => randomBytes(16).toString('hex');

export const gachaPokemon = (req, res, next) => {
  const randomNumber = Math.floor(Math.random() * 2);
  if (randomNumber === 1) {
    next();
  } else {
    responseHelper(res, 201, { catchPokemon: false }, 'Fail to catch pokemon');
  }
};

export const checkRelease = (req, res, next) => {
  const randomNumber = Math.floor(Math.random() * 1000);
  if (isPrime(randomNumber)) {
    res.locals.randomNumber = randomNumber;
    next();
  } else {
    responseHelper(res, 201, { succesRelease: false }, `You got ${randomNumber}! Failed to release pokemon`);
  }
};

export const renamePokemon = (name, release) => {
  const splitName = name.split('-');
  const originalName = splitName[0];
  const fiboKey = fibonacci(release);
  return `${originalName}-${fiboKey}`;
};
