import { readFileSync, writeFileSync } from 'fs';
import last from 'lodash/last.js';

const dbPath = new URL('../Database/movies.json', import.meta.url);

export const getData = () => JSON.parse(readFileSync(dbPath));
export const setData = (data) => writeFileSync(dbPath, JSON.stringify(data));

export const generateMovieId = () => {
  const data = getData();
  const lastMovie = last(data.movies);
  return lastMovie.id + 1;
};
