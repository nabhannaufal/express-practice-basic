import express from 'express';
import pokemonController from '../Controllers/pokemonController.js';
import { gachaPokemon, checkRelease } from '../Helper/pokemonHelper.js';

const pokemonRouter = express.Router();

pokemonRouter.get('/', pokemonController.getPokemonList);
pokemonRouter.get('/mypokemon', pokemonController.getMyPokemon);
pokemonRouter.get('/rename/:id', pokemonController.renamePokemon);
pokemonRouter.get('/detail/:name', pokemonController.getPokemonDetail);
pokemonRouter.get('/catch/:name', gachaPokemon, pokemonController.catchPokemon);
pokemonRouter.get('/release', checkRelease, pokemonController.releasePokemon);

export default pokemonRouter;
