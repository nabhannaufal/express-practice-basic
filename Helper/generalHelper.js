export const isPrime = (num) => {
  if (num <= 1) return false;
  if (num <= 3) return true;

  if (num % 2 === 0 || num % 3 === 0) return false;

  for (let i = 5; i * i <= num; i += 6) {
    if (num % i === 0 || num % (i + 2) === 0) return false;
  }
  return true;
};

export const fibonacci = (index) => {
  if (index === 0) return 0;
  if (index === 1) return 1;

  let prev = 0;
  let current = 1;

  for (let i = 2; i <= index; i++) {
    const next = prev + current;
    prev = current;
    current = next;
  }

  return current;
};
