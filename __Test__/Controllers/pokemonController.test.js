import axios from 'axios';
import pokemonController from '../../Controllers/pokemonController';
import responseHelper from '../../Helper/responseHelper';
import { getMyPokemon, generatePokemonId, renamePokemon, setMyPokemon } from '../../Helper/pokemonHelper';

jest.mock('axios');
jest.mock('../../Helper/responseHelper');
jest.mock('../../Helper/pokemonHelper');

describe('pokemonController', () => {
  let mockReq;
  let mockRes;

  beforeEach(() => {
    mockReq = { params: {} };
    mockRes = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
      locals: {},
    };
    jest.clearAllMocks();
  });

  describe('getPokemonList', () => {
    it('should get a list of Pokemon successfully', async () => {
      const mockResponse = {
        data: {
          results: [{ name: 'Pikachu' }, { name: 'Charmander' }],
        },
      };
      axios.get.mockResolvedValue(mockResponse);

      await pokemonController.getPokemonList(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon?limit=100');
      expect(responseHelper).toHaveBeenCalledWith(
        mockRes,
        200,
        ['Pikachu', 'Charmander'],
        'Succesfully get pokemon list'
      );
    });

    it('should handle error and return 500', async () => {
      axios.get.mockRejectedValue(new Error('Some error'));

      await pokemonController.getPokemonList(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon?limit=100');
      expect(responseHelper).toHaveBeenCalledWith(mockRes, 500);
    });
  });

  describe('getPokemonDetail', () => {
    it('should get Pokemon detail successfully', async () => {
      const mockResponse = {
        data: { name: 'Pikachu', type: 'Electric' },
      };
      axios.get.mockResolvedValue(mockResponse);
      mockReq.params.name = 'Pikachu';

      await pokemonController.getPokemonDetail(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon/Pikachu');
      expect(responseHelper).toHaveBeenCalledWith(
        mockRes,
        200,
        { name: 'Pikachu', type: 'Electric' },
        'Succesfully get pokemon detail'
      );
    });

    it('should handle not found and return 400', async () => {
      const mockError = { response: { status: 404 } };
      axios.get.mockRejectedValue(mockError);
      mockReq.params.name = 'MissingPokemon';

      await pokemonController.getPokemonDetail(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon/MissingPokemon');
      expect(responseHelper).toHaveBeenCalledWith(mockRes, 400, null, `Can't find pokemon with name: MissingPokemon`);
    });

    it('should handle other error and return 500', async () => {
      axios.get.mockRejectedValue(new Error('Some error'));
      mockReq.params.name = 'Pikachu';

      await pokemonController.getPokemonDetail(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon/Pikachu');
      expect(responseHelper).toHaveBeenCalledWith(mockRes, 500);
    });
  });

  describe('getMyPokemon', () => {
    it("should get user's Pokémon successfully", () => {
      const mockMyPokemon = [{ id: 1, name: 'Pikachu' }];
      getMyPokemon.mockReturnValue(mockMyPokemon);

      pokemonController.getMyPokemon(mockReq, mockRes);

      expect(getMyPokemon).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(mockRes, 200, mockMyPokemon, 'Succesfully get my pokemon');
    });

    it('should handle error and return 500', () => {
      getMyPokemon.mockImplementation(() => {
        throw new Error('Some error');
      });

      pokemonController.getMyPokemon(mockReq, mockRes);

      expect(getMyPokemon).toHaveBeenCalled();
      expect(responseHelper).toHaveBeenCalledWith(mockRes, 500);
    });
  });

  describe('catchPokemon', () => {
    it('should catch a new Pokemon successfully', () => {
      const mockMyPokemon = [];
      const mockNewPokemon = { id: 'someId', name: 'Pikachu', release: 0 };
      getMyPokemon.mockReturnValue(mockMyPokemon);
      generatePokemonId.mockReturnValue('someId');
      mockReq.params.name = 'Pikachu';

      pokemonController.catchPokemon(mockReq, mockRes);

      expect(generatePokemonId).toHaveBeenCalled();
      expect(setMyPokemon).toHaveBeenCalledWith([mockNewPokemon]);
      expect(responseHelper).toHaveBeenCalledWith(
        mockRes,
        200,
        { ...mockNewPokemon, catchPokemon: true },
        'Succesfully catch pokemon'
      );
    });

    it('should handle error and return 500', () => {
      getMyPokemon.mockImplementation(() => {
        throw new Error('Some error');
      });

      pokemonController.catchPokemon(mockReq, mockRes);

      expect(responseHelper).toHaveBeenCalledWith(mockRes, 500);
    });
  });

  describe('releasePokemon', () => {
    it('should release a Pokemon successfully', () => {
      mockRes.locals.randomNumber = 123;

      pokemonController.releasePokemon(mockReq, mockRes);

      expect(responseHelper).toHaveBeenCalledWith(
        mockRes,
        200,
        { succesRelease: true },
        'You got 123! Success to release pokemon'
      );
    });

    it('should return error response with status code 500', () => {
      const mockRespose = {};

      pokemonController.releasePokemon({}, mockRespose);

      expect(responseHelper).toHaveBeenCalledWith(mockRespose, 500);
    });
  });

  describe('renamePokemon', () => {
    it('should rename a Pokemon successfully', () => {
      const mockMyPokemon = [{ id: 'someId', name: 'Pikachu', release: 0 }];
      getMyPokemon.mockReturnValue(mockMyPokemon);
      mockReq.params.id = 'someId';
      renamePokemon.mockReturnValue('Pikachu-1');

      pokemonController.renamePokemon(mockReq, mockRes);

      expect(setMyPokemon).toHaveBeenCalledWith([{ id: 'someId', name: 'Pikachu-1', release: 1 }]);
      expect(responseHelper).toHaveBeenCalledWith(
        mockRes,
        200,
        { id: 'someId', name: 'Pikachu-1', release: 1 },
        'Success rename pokemon'
      );
    });

    it('should handle not found and return 400', () => {
      getMyPokemon.mockReturnValue([]);
      mockReq.params.id = 'someId';

      pokemonController.renamePokemon(mockReq, mockRes);

      expect(responseHelper).toHaveBeenCalledWith(mockRes, 400, null, `cant find pokemon with id: someId`);
    });

    it('should handle error and return 500', () => {
      getMyPokemon.mockImplementation(() => {
        throw new Error('Some error');
      });

      pokemonController.renamePokemon(mockReq, mockRes);

      expect(responseHelper).toHaveBeenCalledWith(mockRes, 500);
    });
  });
});
