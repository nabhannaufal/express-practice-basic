import { readFileSync, writeFileSync } from 'fs';
import last from 'lodash/last.js';
import { getData, setData, generateMovieId } from '../../Helper/movieHelper';

jest.mock('fs');
jest.mock('lodash/last.js');

describe('Movie Module', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('getData', () => {
    it('should read and parse data from the file', () => {
      const mockData = { movies: [{ id: 1, title: 'Movie 1' }] };
      readFileSync.mockReturnValue(JSON.stringify(mockData));

      const result = getData();

      expect(result).toEqual(mockData);
      expect(readFileSync).toHaveBeenCalledWith(expect.anything());
    });
  });

  describe('setData', () => {
    it('should write data to the file', () => {
      const mockData = { movies: [{ id: 1, title: 'Movie 1' }] };

      setData(mockData);

      expect(writeFileSync).toHaveBeenCalledWith(expect.anything(), JSON.stringify(mockData));
    });
  });

  describe('generateMovieId', () => {
    it('should generate a new movie id', () => {
      last.mockReturnValue({ id: 3 });
      const mockData = { movies: [{ id: 1, title: 'Movie 1' }] };

      const result = generateMovieId();

      expect(result).toBe(4);
      expect(last).toHaveBeenCalledWith(mockData.movies);
    });
  });
});
