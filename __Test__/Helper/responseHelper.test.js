import responseHelper from '../../Helper/responseHelper';

describe('responseHelper', () => {
  let mockRes;

  beforeEach(() => {
    mockRes = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    jest.clearAllMocks();
  });

  it('should handle success response', () => {
    responseHelper(mockRes, 200, { data: 'test' }, 'Success Message');

    expect(mockRes.status).toHaveBeenCalledWith(200);
    expect(mockRes.json).toHaveBeenCalledWith({
      status: 'Success',
      statusCode: 200,
      message: 'Success Message',
      data: { data: 'test' },
    });
  });

  it('should handle error response with 500 status', () => {
    responseHelper(mockRes, 500, null, 'Error Message');

    expect(mockRes.status).toHaveBeenCalledWith(500);
    expect(mockRes.json).toHaveBeenCalledWith({
      status: 'Error',
      statusCode: 500,
      message: 'Internal Server Error',
    });
  });

  it('should handle error response with 404 status', () => {
    responseHelper(mockRes, 404, null, 'Api Not Found');

    expect(mockRes.status).toHaveBeenCalledWith(404);
    expect(mockRes.json).toHaveBeenCalledWith({
      status: 'Error',
      statusCode: 404,
      message: 'Api Not Found',
    });
  });

  it('should handle other error response', () => {
    responseHelper(mockRes, 400, null, 'Bad Request');

    expect(mockRes.status).toHaveBeenCalledWith(400);
    expect(mockRes.json).toHaveBeenCalledWith({
      status: 'Error',
      statusCode: 400,
      message: 'Bad Request',
    });
  });
});
